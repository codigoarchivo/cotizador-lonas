/**
 * The ById function returns the element with the id that is passed to it.
 * @param id - The id of the element you want to select.
 */
const ById = (id) => document.getElementById(id);

/**
 * `ByCla` is a function that takes a string as an argument and returns the first element in the DOM
 * that matches the string
 * @param cla - The class name of the element you want to select.
 */
const ByCla = (cla) => document.querySelector(cla);

/**
 * It creates an element.
 * @param cre - The element you want to create.
 */
const ByCre = (cre) => document.createElement(cre);

/**
 * It returns the value of the radio button that is checked.
 * @param inp - The name of the radio button group.
 */
const ByInp = (inp) => document.querySelector(`input[name=${inp}]:checked`);

/**
 * ByNam is a function that takes a parameter called nam and returns the result of the
 * document.getElementsByName method with the parameter nam.
 * @param nam - The name of the element you want to get.
 */
const ByNam = (nam) => document.getElementsByName(nam);

/**
 * BySel is a function that takes a selector as an argument and returns the first element that matches
 * the selector.
 * @param sel - The selector you want to use.
 */
const BySel = (sel) => document.querySelector(`${sel}`);

/**
 * The above function is a shorthand for the following:
 *
 * `const bySelAll = function(selAll) { return document.querySelectorAll(selAll); }`
 *
 * The above function is a shorthand for the following
 * @param selAll - The selector you want to use.
 */
const bySelAll = (selAll) => document.querySelectorAll(`${selAll}`);

// ===================================

const isEmptyOneOrVarious = (empty) => {
  const { tipo, dimensiones, formato, acabado, sucursal } = empty;

  if ([acabado, dimensiones, formato, sucursal, tipo].includes(undefined))
    return true;

  return false;
};

const serchFind = (id) => {
  return factura.listFaturas.find((item) => item.id === id);
};

const clearform = () => {
  ById('form').reset();
  form.clearForm();
  clearLienzo();
  clearDivResumen();
};

const clearLienzo = () => {
  const lienzo = new Lienzo();
  lienzo.ctx.clearRect(0, 0, canvas.width, canvas.height);
};

const clearDivResumen = () => {
  ById('resumen').innerHTML =
    '<div class="resumen"><span class="chip">No hay datos...</span></div>';
};

// ===================================

const neto = (data) => {
  return data.reduce((acc, next) => acc + next.total, 0);
};

const inpuesto = (neto) => {
  const tasa = 0.18;
  return ((parseFloat(neto) * tasa) / 1 + tasa).toFixed(2);
};

const parcial = (neto, inpuesto) => {
  return (Number(neto) + Number(inpuesto)).toFixed(2);
};

// ===================================

const isEmpty = (empty) => {
  for (const key in empty) return false;
  return true;
};

/* A function that formats a number to a currency format. */
const format = (value) => {
  const formater = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });
  return formater.format(value);
};

const uuidv4 = () => {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
};
// ===================================

const mostrarOcultarFactura = (data) => {
  if (isEmpty(serch.listClient)) {
    Swal.fire({
      title: 'Error!',
      text: 'No as elegido cliente',
      icon: 'error',
      confirmButtonText: 'Cerrar',
    });
    return;
  }

  if (!factura.listFaturas[0]) {
    Swal.fire({
      title: 'Error!',
      text: 'No as creado Cotización',
      icon: 'error',
      confirmButtonText: 'Cerrar',
    });
    return;
  }

  if (!!data) {
    BySel('.modal-tres').classList.toggle('show-modal');
    toggle('.ripple', '.modal-tres', '#close');
  }
};

const toggle = (button, modal, close) => {
  const toggleModal = () => BySel(`${modal}`).classList.toggle('show-modal');

  if (button !== '.ripple') {
    BySel(`${button}`).addEventListener('click', toggleModal);
  }

  BySel(`${close}`).addEventListener('click', toggleModal);

  window.addEventListener(
    'click',
    ({ target }) => target === BySel(`${modal}`) && toggleModal()
  );
};

toggle('.trigger', '.modals-dos', '.close-button');
toggle('.agregar-formato', '.modals-uno', '.formato-close');

// ===================================

const validarSerch = (id, d) => (ById(`${id}`).style.display = `${d}`);

const noClient = () => {
  ById('encontro').innerHTML = '<h1>No hay coincidencias</h1>';
  setTimeout(() => {
    clearText('encontro');
    clearText('Buscador');
  }, 2000);
};

// ===================================

const clearText = (data) => (ById(`${data}`).innerHTML = '');

const clearValue = (data) => (ById(`${data}`).value = '');

const clearTextSelect = (data) => (BySel(`${data}`).value = '');

// ===================================

window.onscroll = function () {
  myFunction();
};

const sticky = ByCla('.pantalla').offsetTop;

const myFunction = () => {
  const data = ByCla('.pantalla').classList;
  window.pageYOffset > sticky ? data.add('sticky') : data.remove('sticky');
};
