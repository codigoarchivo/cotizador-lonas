class Forms {
  constructor() {
    this.resultado = {};
    this.total = 0;
  }

  get referencia() {
    return {
      ...this.resultado,
      cantidad: 1,
      total: this.total,
      id: uuidv4(),
    };
  }

  pantalla() {
    ById("resumen").innerHTML = '';
    const div = ByCre('div');
    div.classList = 'resumen';

    const { tipo, dimensiones, formato, acabado, sucursal } = this.resultado;

    isEmpty(this.resultado)
      ? (div.innerHTML = "<span class='chip'>No hay datos...</span>")
      : (div.innerHTML = `
     ${tipo ?? "<span class='chip'>Agrega tipo de lona</span>"} 
     ${dimensiones ?? "<span class='chip'>Agrega dimensiones</span>"} 
     ${formato ?? "<span class='chip'>Agrega tipo de formato</span>"} 
     ${acabado ?? "<span class='chip'>Agrega el, los acabados</span>"} 
     ${sucursal ?? "<span class='chip'>Agrega la sucursal</span>"} 
     ${this.total ? "<p><b> Total: " + format(this.total) + "</b> </p>" : ""}
    `);

    ById("resumen").appendChild(div);
  }

  tipoYPrecioDeLona() {
    const { options, selectedIndex } = ById('tipoLona');
    const lona = options[selectedIndex];

    if (!lona.getAttribute('data-value')) return;

    if (lona) {
      const price = Number(lona.value);
      this.resultado = {
        ...this.resultado,
        tipo:
          `<p>${lona.getAttribute(
            'data-value'
          )} con un precio por metro de ${format(price)}</p>` || undefined,
      };
      this.total = price;
    }
  }

  medidaYprecioDeLona() {
    const ancho = ById('medida_largo');
    const alto = ById('medida_alto');
    const medida = ById('medida_total');

    if (!medida.value) return;

    const lienzo = new Lienzo();
    lienzo.draw(alto.value, ancho.value);

    if (medida) {
      const calc = Number((medida.value = alto.value * ancho.value)) || 0;

      if (!calc) return;

      this.resultado = {
        ...this.resultado,
        dimensiones:
          `<p>Dimensiones de ${ancho.value} x ${alto.value} es ${calc}m²</p> ` ||
          undefined,
      };
      this.total *= calc;
    }
  }

  tipoDeImpresion() {
    const formato = ByInp('formatos');

    if (!formato) return;

    if (formato) {
      const price = Number(formato.value);

      this.resultado = {
        ...this.resultado,
        formato:
          `<p>${formato.getAttribute('data-value')} tiene un precio de ${format(
            price
          )}</p>` || undefined,
      };
      this.total += price;
    }
  }

  tipoDeAcabado() {
    const acabados = ByNam('acabados');
    let acabado = [];
    let precio = 0;

    if (!acabados) return;

    for (let i = 0; i < acabados.length; i++) {
      if (acabados[i].checked) {
        acabado.push(acabados[i].getAttribute('data-value'));
        precio += Number(acabados[i].value);
      }
    }

    if (!precio && !acabado) return;

    this.resultado = {
      ...this.resultado,
      acabado: !!acabado[0]
        ? `<p>Con acabados de ${acabado.join(', ')}</p> `
        : undefined,
    };
    this.total += precio;
    acabado = [];
  }

  tipoSucursales() {
    const sucursal = ByInp('sucursales');

    if (!sucursal) return;

    this.resultado = {
      ...this.resultado,
      sucursal:
        `<p>A entregar en la Sucursal ${sucursal.value}</p>` || undefined,
    };
  }

  clearForm() {
    this.resultado = {};
    this.resultado = {};
    this.total = 0;
  }
}
