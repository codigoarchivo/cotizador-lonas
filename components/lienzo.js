class Perimeters {
  ancho;
  alto;

  constructor() {
    this.x = 0;
    this.y = 0;
  }

  get ctx() {
    return canvas.getContext("2d");
  }

  widthAndHeight(an, al) {
    const lienzo = canvas.width - 20;
    const mayor = Math.max(an, al);

    switch (parseInt(mayor)) {
      case parseInt(an):
        this.ancho = this.calculateLength(an, lienzo);
        this.alto = lienzo * (al / an);
        break;

      case parseInt(al):
        this.alto = this.calculateLength(al, lienzo);
        this.ancho = lienzo * (an / al);
        break;
    }

    return {
      alto: this.alto,
      ancho: this.ancho,
    };
  }

  calculateLength(data, lienzo) {
    return parseInt(data) + (parseInt(lienzo) - parseInt(data));
  }

  centralPoint(ancho, alto) {
    this.x = canvas.width / 2 - ancho / 2;
    this.y = canvas.width / 2 - alto / 2;
  }

  theEdges(ancho, alto) {
    this.ctx.fillStyle = "#ee1c25";
    this.ctx.fillRect(this.x + 2, this.y + 2, ancho, alto);
  }

  theAreas(ancho, alto) {
    this.ctx.fillStyle = "#010000";
    this.ctx.fillRect(this.x, this.y, ancho, alto);
  }

  queryMatches() {
    const { matches } = window.matchMedia("(max-width: 480px)");
    if (matches) {
      canvas.width = 200;
      canvas.height = 200;
    } else {
      canvas.width = 250;
      canvas.height = 250;
    }
  }
}

class Lienzo extends Perimeters {
  draw(al = 0, an = 0) {
    /* Clearing the canvas. */
    super.ctx.clearRect(0, 0, canvas.width, canvas.height);

    super.queryMatches();

    /* Calling the method widthAndHeight of the class Lienzo. */
    const { ancho, alto } = super.widthAndHeight(an, al);

    /* Calling the centralPoint method of the Perimeters class. */
    super.centralPoint(ancho, alto);

    /* Calling the method theEdges of the class Perimeters. */
    super.theEdges(ancho, alto);

    /* Calling the method theAreas of the class Perimeters. */
    super.theAreas(ancho, alto);
  }
}
