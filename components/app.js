ById("form").addEventListener("submit", (e) => {
  e.preventDefault();

  if (isEmpty(form.referencia)) {
    clearform();
    factura.calcular({});
    Swal.fire({
      title: "Error!",
      text: "Todos los campos están vacíos",
      icon: "error",
      confirmButtonText: "Cerrar",
    });
    return;
  }

  if (isEmptyOneOrVarious(form.referencia)) {
    clearform();
    factura.calcular({});
    Swal.fire({
      title: "Error!",
      text: "Faltan algunos campos comienza de nuevo",
      icon: "error",
      confirmButtonText: "Cerrar",
    });
    return;
  }

  factura.calcular(form.referencia, "POST");
  factura.pantalla();
  factura.pantallaTotal();
  clearform();

  Swal.fire({
    icon: "success",
    title: "Your work has been saved",
    showConfirmButton: false,
    timer: 1500,
  });
});

ById("form").addEventListener("change", () => {
  form.tipoYPrecioDeLona();
  form.medidaYprecioDeLona();
  form.tipoDeImpresion();
  form.tipoDeAcabado();
  form.tipoSucursales();
  form.pantalla();
});

const increment = (data) => {
  const { cantidad, id, base } = serchFind(data.getAttribute("data-id"));

  const increment = cantidad + 1;

  const newData = {
    id,
    cantidad: increment,
    total: base * increment,
  };
  factura.calcular(newData, "PUT");
  factura.pantalla();
  factura.pantallaTotal();
};

const decrement = (data) => {
  const { cantidad, id, base } = serchFind(data.getAttribute("data-id"));

  const decrement = cantidad - 1;

  if (decrement === 0) return;

  const newData = {
    id,
    cantidad: decrement,
    total: base * decrement,
  };
  factura.calcular(newData, "PUT");
  factura.pantalla();
  factura.pantallaTotal();
};

const handleDelete = (data) => {
  const del = data.getAttribute("data-id");
  factura.calcular(del, "DELETE");
  ById(del).remove();
  factura.pantalla();
  factura.pantallaTotal();
};

const handleSerch = (data) => {
  if (data.value === "") return volver();

  if (serch.listClients.length < 11) serch.baseClient(clients);

  const serchData = serch.serchData(data);

  if (!serchData[0]) return noClient();

  serch.pantalla(serchData);
  validarSerch("formModal", "none");
  validarSerch("searchClose", "block");
};

const selectClient = (data) => {
  const id = data.getAttribute("data-id");
  const client = serch.selectSerch(id);
  serch.perClient(client);
  serch.pantallaClient();
};

const volver = () => {
  validarSerch("formModal", "block");
  validarSerch("searchClose", "none");
  clearValue("Buscador");
  clearText("encontro");
};
