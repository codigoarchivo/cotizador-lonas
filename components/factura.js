class Config {
  constructor() {
    this.facturas = [];
  }

  get listFaturas() {
    return (this.facturas = JSON.parse(localStorage.getItem('facturas')) || []);
  }

  recibido(factura) {
    if (!factura) return;
    localStorage.setItem(
      'facturas',
      JSON.stringify([factura, ...this.facturas])
    );
  }

  deleteRecibido(data) {
    const newData = this.facturas.filter((item) => item.id !== data);
    localStorage.setItem('facturas', JSON.stringify(newData));
  }

  updateRecibido(data) {
    const factura = this.facturas.map((item) => {
      if (item.id === data.id) {
        return {
          ...item,
          cantidad: data.cantidad,
          total: data.total,
        };
      }
      return item;
    });
    localStorage.setItem('facturas', JSON.stringify(factura));
  }
}

class Factura extends Config {
  clearRecibido() {
    super.facturas = [];
    localStorage.clear('facturas');
  }

  calcular(data, method) {
    switch (method) {
      case 'POST':
        super.recibido(data);
        break;
      case 'DELETE':
        super.deleteRecibido(data);
        break;
      case 'PUT':
        super.updateRecibido(data);
        break;

      default:
        [];
        break;
    }
  }

  pantalla() {
    ById('tbody').innerHTML = '';
    super.listFaturas?.map((item, key) => {
      const tr = ByCre('tr');
      tr.id = item.id;
      tr.innerHTML = `
                  <td>${key + 1}</td>
                  <td>
                  <div>
                    <span>Tipo: </span>
                    ${item.tipo}
                  </div>
                   <div> 
                    <span>Dimensiones: </span>
                    ${item.dimensiones}
                   </div>
                   <div> 
                    <span>Formato: </span>
                    ${item.formato}
                   </div>
                   <div> 
                    <span>Acabado: </span>
                    ${item.acabado}
                   </div>
                   <div> 
                    <span>Sucursal: </span>
                    ${item.sucursal}
                   </div>
                   <div>
                      <span>Total: </span>
                      <span> ${format(item.total)}</span>
                   </div>
                  </td>
                
                  <td>
                    <button 
                      type='button'
                      data-id='${item.id}'
                      onclick='decrement(this)'
                      >-</button>
                    <span>${item.cantidad}</span>
                    <button 
                      type='button'
                      data-id='${item.id}'
                      onclick='increment(this)'
                      >+</button>
                </td>
                  <td>
                    <button 
                      class='delete'
                      type='button'
                      data-id='${item.id}'
                      onclick='handleDelete(this)'
                      >x</button>
                  </td>`;

      ById('tbody').appendChild(tr);
    });
  }

  pantallaTotal() {
    const n = neto(super.listFaturas);
    const i = inpuesto(n);
    const p = parcial(n, i);

    ById('tfoot').innerHTML = `
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Parcial</td>
        <th><span id='parcial' class='ocultar'>${format(n ? n : 0)}</span></th>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>0.18%</td>
        <th><span id='inpuesto' class='ocultar'>${format(i ? i : 0)}</span></th>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Total</td>
        <th><span id='neto'>${format(p ? p : 0)}</span></th>
    </tr>`;
  }
}
