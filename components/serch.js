class Serch {
  constructor() {
    this.clients = [];
  }
  
  get listClients() {
    return (this.clients = JSON.parse(localStorage.getItem("clients")) || []);
  }

  get listClient() {
    return JSON.parse(localStorage.getItem("client")) || {};
  }

  baseClient(clients) {
    const data = clients.map((item) => {
      return { ...item, id: uuidv4() };
    });
    return localStorage.setItem("clients", JSON.stringify(data));
  }

  perClient(client) {
    if (!client) return;
    return localStorage.setItem("client", JSON.stringify(client));
  }

  serchData(data) {
    const client = data.value.trim().toLowerCase();
    const serchData = this.clients.filter((item) => {
      if (item.name.toLowerCase().startsWith(client)) return item;
      if (item.nPhone.startsWith(client)) return item;
      if (item.document.startsWith(client)) return item;
    });
    return serchData;
  }

  selectSerch(id) {
    const serchData = this.clients.find((item) => item.id === id.toString());
    return serchData;
  }

  pantalla(data) {
    clearText("encontro");
    data?.map((item) => {
      const { name, lastName, nPhone, document, id } = item;
      const div = ByCre("div");
      div.classList = "serch";
      div.innerHTML = `
     <div>
      ${"<p>" + name + "</p>"} 
      ${"<p>" + lastName + "</p>"}
      ${"<p>" + nPhone + "</p>"}
      ${"<p>" + document + "</p>"}
     </div>
     <button 
      data-id="${id}"
      onclick="selectClient(this)"
     >enviar</button>
    `;
      ById("encontro").append(div);
    });
  }

  pantallaClient() {
    clearTextSelect(".empresa");
    const { name, lastName, nPhone, document } = this.listClient;
    BySel(".empresa").innerHTML = `
    <div>
        <th>Nombre: </th>
        <td>${name}</td>
    </div>
    <div>
        <th>Cedula: </th>
        <td>${lastName}</td>
    </div>
    <div>
        <th>Telefono: </th>
        <td>${nPhone}</td>
    </div>
    <div>
        <th>CI: </th>
        <td>${document}</td>
    </div>
     `;
  }
}
