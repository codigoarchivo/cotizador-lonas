const serch = new Serch();
const form = new Forms();
const factura = new Factura();

document.addEventListener("DOMContentLoaded", () => {
  form.pantalla();
  factura.pantalla();
  factura.pantallaTotal();
  serch.pantalla();
  serch.pantallaClient();
  mostrarOcultarFactura()
});
