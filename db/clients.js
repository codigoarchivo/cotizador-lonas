const clients = [
  {
    name: "Juan",
    lastName: "Valero",
    nPhone: "41265875151",
    document: "54415625",
  },
  {
    name: "Miguel",
    lastName: "Roberto",
    nPhone: "41254522266",
    document: "45292964",
  },
  {
    name: "Daniel",
    lastName: "Josefina",
    nPhone: "415454554576",
    document: "592226513",
  },
  {
    name: "Francis",
    lastName: "Grabiela",
    nPhone: "414115156226",
    document: "4812662622",
  },
  {
    name: "Alejandro",
    lastName: "Delgado",
    nPhone: "424526260662",
    document: "85416265416",
  },
  {
    name: "Julian",
    lastName: "Alvares",
    nPhone: "422854625895",
    document: "895154775523",
  },
  {
    name: "Cristina",
    lastName: "Fernandez",
    nPhone: "4168462262442",
    document: "8952485526296",
  },
  {
    name: "Luis",
    lastName: "Hernandez",
    nPhone: "41256582226996",
    document: "8546268455215",
  },
  {
    name: "Luis daniel",
    lastName: "Hernandez Juarez",
    nPhone: "699869596990059",
    document: "7537933048850",
  },
  {
    name: "Milena",
    lastName: "Gonzalez",
    nPhone: "41287621332294",
    document: "5222112397562",
  },
];
